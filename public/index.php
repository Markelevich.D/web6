<?php
header('Content-Type:text/html;charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages=array();
    $errors = array();
    $values = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save','',100000);
        setcookie('login', '',100000);
        setcookie('pass', '', 100000);
        $messages['save'] = 'Спасибо, результаты сохранены!';
        if (!empty($_COOKIE['pass']) && empty($_SESSION['login'])) {
            $messages['login_and_password'] = sprintf('Вы можете <a href="login.php">осуществить вход</a> по логину <strong>%s</strong>
            и паролю <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
          }
    }
    
    $flag=FALSE;
    $super_separated='';
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['countlimbs'] = !empty($_COOKIE['countlimbs_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);
    $errors['checkvajno'] = !empty($_COOKIE['checkvajno_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
if ($errors['name']) {
        if($_COOKIE['name_error']=='none'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Ошибка: Не введено имя.</div>';
    }
    if($_COOKIE['name_error']=='Unacceptable symbols'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Ошибка: Недопустимые символы в имени:а-я,А-Я,0-9,\ - _ </div>';
        }
    }
    if ($errors['email']) {
        if($_COOKIE['email_error']=='none'){
            setcookie('email_error','',100000);
            $messages['email'] = '<div class="error">Ошибка: Не указана почта.</div>';
        }
        if($_COOKIE['email_error']=='invalid address'){
            setcookie('email_error','',100000);
            $messages['email'] = '<div class="error">Ошибка: Почта указана некорректно.Пример:email@yandex.ru</div>';
        }
    }
    if($errors['date']){
        setcookie('date_error','',100000);
        $messages['date'] = '<div class="error">Ошибка: Не указан год рождения.</div>';
    }
    if($errors['sex']){
            setcookie('sex_error','',100000);
            $messages['sex'] = '<div class="error">Ошибка: Не указан пол.</div>';
    }
    if ($errors['countlimbs']) {
        setcookie('countlimbs_error','',100000);
        $messages['countlimbs'] = '<div class="error">Ошибка: Не указано количество конечностей.</div>';
    }
    if($errors['super']){
        if($_COOKIE['super_error']=="none"){
            setcookie('super_error','',100000);
            $messages['super'] = '<div class="error">Ошибка: Не выбраны способности.</div>';
        }
        if($_COOKIE['super_error']=="noneselected"){
            setcookie('super_error','',100000);
            $messages['super'] = '<div class="error">Ошибка: Не выбраны способности.</div>';
        }
    }
    if ($errors['biography']) {
            setcookie('biography_error','',100000);
            $messages['biography'] = '<div class="error">Ошибка: Не введена биография.</div>';

    }
    if($errors['checkvajno']){
        setcookie('checkvajno_error','',100000);
        $messages['checkvajno'] = '<div class="error">Ошибка: Не подтверждено согласие.</div>';
    }
    $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['countlimbs'] = empty($_COOKIE['countlimbs_value']) ? '' : strip_tags($_COOKIE['countlimbs_value']);
    $values['super'] = empty($_COOKIE['super_value']) ? '' : strip_tags($_COOKIE['super_value']);
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    $values['checkvajno'] = empty($_COOKIE['checkvajno_value']) ? '' : strip_tags($_COOKIE['checkvajno_value']);
    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        $user = 'u20313';
        $password = '9495788';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20313', $user, $password,
        array(PDO::ATTR_PERSISTENT => true));
        try{
        $stmt = $db->prepare("SELECT name,email,birth,sex,countlimbs,super,bio,checkvajno FROM info WHERE login = '$log'");
        $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
            {
                 $values['name']=$row['name'];
                 $values['email']=$row['email'];
                 $values['date']=$row['birth'];
                 $values['sex']=$row['sex'];
                 $values['countlimbs']=$row['countlimbs'];
                 $values_f=array();
                 if(!empty($row['super'])){
                    if(stristr($row['super'],'net') == TRUE) {
                      array_push($values_f,'net');
                    }
                    if(stristr($row['super'],'unvisibility') == TRUE) {
                      array_push($values_f,'unvisibility');
                    }
                    if(stristr($row['super'],'wallhack') == TRUE) {
                      array_push($values_f,'wallhack');
                    }
                    if(stristr($row['super'],'mindcontrol') == TRUE) {
                      array_push($values_f,'mindcontrol');
                    }
                    if(stristr($row['super'],'changebody') == TRUE) {
                      array_push($values_f,'changebody');
                    }
                    if(stristr($row['super'],'changereality') == TRUE) {
                      array_push($values_f,'changereality');
                    }
                    $values['super']=serialize($values_f);
                  }
                 $values['biography']=$row['bio'];
                 $values['checkvajno']=$row['checkvajno'];
            }
    }catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        } 
    }
    include('form.php');
}
else {
    $errors = FALSE;
            if (empty($_POST['name'])) {
                setcookie('name_error', 'none', time() + 24 * 60 * 60);
                setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else {
                if (!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST['name'])){
                    setcookie('name_error', 'Unacceptable symbols', time() + 24 * 60 * 60);
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                    $errors=TRUE;
                }else{
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                }
            }
          if (empty($_POST['email'])) {
                setcookie('email_error', 'none', time() + 24 * 60 * 60);
                setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,10})$/i", $_POST['email'])) {
                    setcookie('email_error', 'invalid address', time() + 24 * 60 * 60);
                    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['date'])) {
                setcookie('date_error', 'none', time() + 24 * 60 * 60);
                setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['sex'])) {
                setcookie('sex_error', 'none', time() + 24 * 60 * 60);
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['countlimbs'])) {
                setcookie('countlimbs_error', 'none', time() + 24 * 60 * 60);
                setcookie('countlimbs_value', $_POST['countlimbs'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('countlimbs_value', $_POST['countlimbs'], time() + 30 * 24 * 60 * 60);
            }
            if(!isset($_POST['super'])){
                setcookie('super_error', 'none', time() + 24 * 60 * 60);
                setcookie('super_value', serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                $super_mass=$_POST['super'];
                $flag=FALSE;
                for($w=0;$w<count($super_mass);$w++){
                    if($super_mass[$w]=="net"){
                        $flag=TRUE;break;
                    }
                }
                if($flag && count($super_mass)!=1){
                    setcookie('super_error', 'noneselected', time() + 24 * 60 * 60);
                    setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                }
            }
			if (empty($_POST['biography'])) {
                setcookie('biography_error', 'none', time() + 24 * 60 * 60);
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
             }
            if (empty($_POST['checkvajno'])) {
                setcookie('checkvajno_error', 'none', time() + 24 * 60 * 60);
                setcookie('checkvajno_value', $_POST['checkvajno'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('checkvajno_value', $_POST['checkvajno'], time() + 30 * 24 * 60 * 60);
            }
            if ($errors) {
                header('Location:index.php');
                exit();
            }
            else {/*удаляем куки с ошибками*/
                setcookie('name_error', '', 100000);setcookie('countlimbs_error', '', 100000);
                setcookie('email_error', '', 100000);setcookie('super_error', '', 100000);
                setcookie('date_error', '', 100000);setcookie('biography_error', '', 100000);
                setcookie('sex_error', '', 100000);setcookie('checkvajno_error', '', 100000);
            }

       if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])  && !empty($_COOKIE['login'])) 
        {//перезапись по логину
        $user = 'u20313';
        $password = '9495788';
        $super_separated='';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20313', $user, $password,array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("UPDATE info SET name=?,email=?,birth=?,sex=?,countlimbs=?,super=?,bio=?,checkvajno=? WHERE login='$log' ");
        
        $name=$_POST["name"];
        $email=$_POST["email"];
        $birth=$_POST["date"];
        $sex=$_POST["sex"];
        $countlimbs=$_POST["countlimbs"];
        if(!empty($_POST['super'])){
            $super_mass=$_POST['super'];
            for($w=0;$w<count($super_mass);$w++){
                if($flag){
                    if($super_mass[$w]!="net")unset($super_mass[$w]);
                    $super_separated=implode(' ',$super_mass);
                }else{
                    $super_separated=implode(' ',$super_mass);
                }
            }
        }
        $super=$super_separated;
        $bio=$_POST["biography"];
        $checkvajno=$_POST["checkvajno"];
        
        $stmt->execute(array($name,$email,$birth,$sex,$countlimbs,$super,$bio,$checkvajno,));
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }else 
    {   
        $logins_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pass_chars='0123456789abcdefghijklmnopqrstuvwxyz-_';
        $login = substr(str_shuffle($logins_chars), 0, 3);
        $password =substr(str_shuffle($pass_chars),0,3);
        setcookie('login', $login);
        setcookie('pass', $password);
        if(!empty($_POST['super'])){
            $super_mass=$_POST['super'];
            for($w=0;$w<count($super_mass);$w++){
                if($flag){
                    if($super_mass[$w]!="net")unset($super_mass[$w]);
                    $super_separated=implode(' ',$super_mass);
                }else{
                    $super_separated=implode(' ',$super_mass);
                }
            }
        }
        $user = 'u20313';
        $pass = '9495788';
        $db = new PDO('mysql:host=localhost;dbname=u20313', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("INSERT INTO info(name,login,password,email, birth, sex, countlimbs,super,bio,checkvajno) 
        VALUES (:name,:login,:password,:email, :birth, :sex, :countlimbs,:super,:bio, :checkvajno)");
        $stmt->bindParam(':name', $name_db);
        $stmt->bindParam(':login', $login_db);
        $stmt->bindParam(':password', $pass_db);
        $stmt->bindParam(':email', $email_db);
        $stmt->bindParam(':birth', $date_db);
        $stmt->bindParam(':sex', $sex_db);
        $stmt->bindParam(':countlimbs', $countlimbs_db);
        $stmt->bindParam(':super', $super_db);
        $stmt->bindParam(':bio', $bio_db);
        $stmt->bindParam(':checkvajno', $checkvajno_db);
        $name_db=$_POST["name"];
        $login_db=$login;
        $pass_db=md5($password);
        $email_db=$_POST["email"];
        $date_db=$_POST["date"];
        $sex_db=$_POST["sex"];
        $countlimbs_db=$_POST["countlimbs"];
        $super_db=$super_separated;
        $bio_db=$_POST["biography"];
        $checkvajno_db=$_POST["checkvajno"];
        $stmt->execute();
        }
        catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
            }
        }
    setcookie('save', '1');
    header('Location:index.php');
}
?>
